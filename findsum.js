var arr = [1,4,678,9,6,-6];

//imperative style  
function findSumImperative(arr){
    let i = 0;
    var n = arr.length;
    var sum = 0;
    for(i = 0;i<n;i++){
        sum = sum+arr[i];
    }
    return sum;
}

// declarative
function findSumReduce(arr){

    return arr.reduce(function(sum,curr){
        return sum+curr;
    },0);

}

// declarative fat arrow
function findSumReduce1(arr){

    return arr.reduce((sum,curr) => sum+curr,0);

}
