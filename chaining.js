function Number(num){
	this.num = num;
}

Number.prototype.sum = function(x){
	this.num = this.num +x;
  return this;
}

Number.prototype.minus = function(x){
	this.num = this.num -x;
  return this;
}

Number.prototype.multiply = function(x){
	this.num = this.num*x;
  return this;
}

Number.prototype.display = function(){
	console.log(this.num);
}

new Number(5).sum(7).minus(2).multiply(4).display();